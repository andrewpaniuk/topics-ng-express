import User from "./users.model";

export default class Comment {
    _id: string;
    text: string;
    creater: User;
    created: Date;
}