import Comment from './comments.model';

export default class Topic {
    _id: string;
    name: string;
    created: Date;
    comments: Comment[];
}