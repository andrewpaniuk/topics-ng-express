import Comment from "./comments.model";

export default class User {
    _id: string;
    name: string;
    comments: Comment[];
}