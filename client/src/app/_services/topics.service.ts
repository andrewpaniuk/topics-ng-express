import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Response } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import Topic from '../_models/topics.model';

@Injectable({
    providedIn: 'root'
})
export class TopicsService {

    apiURL = 'http://localhost:3000';
    topicsURL = `${this.apiURL}/api/topics`

    constructor(private _http: HttpClient) { }

    getTopics(): Observable<Topic[]> {
        return this._http.get(this.topicsURL)
            .pipe(map((data: Topic[]) => {
                return data;
            }));
    }

    getOneTopic(id: string) {
        return this._http.get(`${this.topicsURL}/${id}`)
            .pipe(map((data: Topic) => {
                return data;
            }));;
    }
}
