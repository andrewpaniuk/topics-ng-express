import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { TopicsComponent } from './topics/topics.component';

const routes: Routes = [
    {path: '', redirectTo: '/topics', pathMatch: 'full'},
    {path: 'topics', component: TopicsComponent}
];


@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  declarations: [],
  exports: [RouterModule]
})
export class AppRoutingModule { }
