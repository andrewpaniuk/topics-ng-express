import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';

import { AppRoutingModule } from './app-routing.module';
import { TopicsModule } from './topics/topics.module';
import { TopicsService } from './_services/topics.service';

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        TopicsModule,
        AppRoutingModule
    ],
    providers: [TopicsService],
    bootstrap: [AppComponent]
})
export class AppModule { }
