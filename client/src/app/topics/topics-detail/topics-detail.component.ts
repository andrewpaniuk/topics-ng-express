import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import Topic from '../../_models/topics.model';
import { TopicsService } from '../../_services/topics.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-topics-detail',
    templateUrl: './topics-detail.component.html',
    styleUrls: ['./topics-detail.component.sass']
})
export class TopicsDetailComponent implements OnInit {

    id: string;
    topic: Topic;
    hours;
    minutes;
    seconds;

    sub: Subscription

    constructor(private _route: ActivatedRoute,
        private _topicsService: TopicsService) { }

    ngOnInit() {
        this._route.params.subscribe(params => {
            this.id = params.id;

            this.sub = this._topicsService.getOneTopic(this.id)
                .subscribe(data => {
                    this.topic = data;
                    let date = new Date(data.created);
                    this.hours = date.getHours();
                    this.minutes = date.getMinutes();
                    this.seconds = date.getSeconds();
                    console.log(this.topic);
                });
        })
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }

}
