import { Component, OnInit, Input } from '@angular/core';
import Topic from '../../../_models/topics.model';

@Component({
    selector: '.topic-item',
    templateUrl: './topics-item.component.html',
    styleUrls: ['./topics-item.component.sass']
})
export class TopicsItemComponent implements OnInit {
    @Input() topic: Topic;

    constructor() { }

    ngOnInit() {
    }

}
