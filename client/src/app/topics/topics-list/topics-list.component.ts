import { Component, OnInit } from '@angular/core';
import Topic from '../../_models/topics.model';
import { TopicsService } from '../../_services/topics.service';

@Component({
    selector: 'app-topics-list',
    templateUrl: './topics-list.component.html',
    styleUrls: ['./topics-list.component.sass']
})
export class TopicsListComponent implements OnInit {

    topics: Topic[];

    constructor(private _topicsService: TopicsService) { }

    ngOnInit() {
        this._topicsService.getTopics()
            .subscribe(topics => this.topics = topics);
    }

}
