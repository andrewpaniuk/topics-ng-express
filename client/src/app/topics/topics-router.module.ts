import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TopicsComponent } from './topics.component';
import { TopicsListComponent } from './topics-list/topics-list.component';
import { TopicsDetailComponent } from './topics-detail/topics-detail.component';
import { TopicsItemComponent } from './topics-list/topics-item/topics-item.component';
import { TopicsFormComponent } from './topics-form/topics-form.component';

const routes: Routes = [
    { path: 'topics', component: TopicsComponent, children: [
        // {path: '', component: TopicsDetailComponent},
        {path: ':id', component: TopicsDetailComponent},
    ] }
];

export const routes_components = [
    TopicsComponent,
    TopicsListComponent,
    TopicsDetailComponent,
    TopicsItemComponent,
    TopicsFormComponent
]

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    declarations: [],
    exports: [RouterModule]
})
export class AppRoutingModule { }
