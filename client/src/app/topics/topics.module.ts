import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule, routes_components } from './topics-router.module';



@NgModule({
    imports: [
        CommonModule,
        AppRoutingModule
    ],
    declarations: [
        ...routes_components
    ]
})
export class TopicsModule { }
