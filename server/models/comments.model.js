const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const comment_schema = new Schema({
    text: {
        type: String,
        requared: true
    },
    creater: {
        type: Schema.Types.ObjectId,
        ref: 'users',
        required: true
    },
    created: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('comments', comment_schema);