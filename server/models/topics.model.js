const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const topic_schema = new Schema({
    name: {
        type: String,
        requared: true
    },
    comments: [{
        type: Schema.Types.ObjectId,
        ref: 'comments'
    }],
    created: {
        type: Date,
        default: new Date()
    }
});

module.exports = mongoose.model('topics', topic_schema);
