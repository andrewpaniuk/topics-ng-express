const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const user_schema = new Schema({
    name: {
        type: String,
        requared: true
    },
    comments: [{
        type: Schema.Types.ObjectId,
        ref: 'comments'
    }]
});

module.exports = mongoose.model('users', user_schema);