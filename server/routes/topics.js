const express = require('express');
const router = express.Router();

const Topic = require('../models/topics.model');

router.get('/topics', (req, res) => {

    Topic.find()
        .then(topics => {
            res.json(topics);
        });
    
});
router.get('/topics/:id', (req, res) => {

    Topic.findById(req.params.id)
        .then(topic => {
	    // console.log(topic);
            res.json(topic);
        });
    
});

module.exports = router;
